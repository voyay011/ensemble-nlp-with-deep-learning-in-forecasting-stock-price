# Ensemble NLP with Deep Learning in Forecasting Stock Price

## Objective
- Predict price is one of the most important phases in technical analysis in the Stock Market. In order to capture all of the information about that, humans need to analyze a large amount of information in a short amount of time.

- By using Natural Language Processing (NLP) to understand the financial statement of a company as well as analyze the news about a specific market. Combine with the Deep Learning model to predict the movement of the price. After that put them together to create an ensemble model (transfer learning) that applying in forecasting the stock price

- The final result will be a web-app that using Heroku or Github to deploy the model and use it to predict real-time stock price base on user input or crawl from the internet. 

## Folder Constructor
~~~
/docs: contains documentations and instruction on how to use the system 
/data: contains dataset for the model
/model: model that has been test and ready to use
/scripts: contains source code and scripts for the system
    /web-app: using streamlit as the front-end for the web-app
    /component: other component that could be implement with the system.
    /devops: using devops for integrated and deploy the model
~~~
