import streamlit as st

st.title("Ensemble Modelling to Forecasting Stock Price Market")

with st.sidebar:
    st.header("Navigation")
    option = st.radio("Choose one:", ["Introduction","Objective & Related Work", "Architecture", "Modelling & Result"])

if option == "Introduction":
    st.write("Predicting Stock Price has always been a difficult task in Machine Learning general and in Fintech specifically. Using ensemble modelling such as the __Long-Short Term Memory (LSTM)__ for getting stock price and __Natural Language Processing (NLP)__ on sentiment classification about stock news. This will increase the accuracy of the prediction.") 
    st.write("There are 3 main tasks : __Stock Price -> Sentiment Analysis News -> Final Prediction__")
    st.write("With each of the step, different models will use and at the end, the result from two models will be use to get the final predictions, treated as a __Decision Tree__")
    st.markdown("For source code, please refer to [Gitlab](https://gitlab.com/voyay011/ensemble-nlp-with-deep-learning-in-forecasting-stock-price).")


